Name:           libmpeg2
Version:        0.5.1
Release:        19
Summary:        The decoder libraries for MPEG-2
License:        GPLv2+
URL:            http://libmpeg2.sourceforge.net
Source0:        http://libmpeg2.sourceforge.net/files/libmpeg2-%{version}.tar.gz
Patch0000:      libmpeg2-inline.patch

BuildRequires:  SDL-devel libXt-devel libXv-devel automake libtool

%description
Libmpeg2 is a free library for decoding mpeg-2 and mpeg-1 video streams.
It is released under the terms of the GPL license.

%package -n     mpeg2dec
Summary:        The decoder program for MPEG-2
Requires:       %{name} = %{version}-%{release}

%description -n mpeg2dec
The libmpeg2-devel package contains libraries and header files for developing applications.

%package        devel
Summary:        Development files for libmpeg2
Requires:       %{name} = %{version}-%{release}

%description    devel
The libmpeg2-devel package contains libraries and header files for developing applications.

%prep
%autosetup -n libmpeg2-%{version} -p1
autoreconf -fiv

iconv -f ISO-8859-1 -t UTF-8 AUTHORS > AUTHORS.tmp
touch -r AUTHORS AUTHORS.tmp
cp -p -f AUTHORS.tmp AUTHORS
rm AUTHORS.tmp
sed -i -e 's/ppc-/noppc64-/' configure.ac configure
sed -i -e 's/powerpc-/nopowerpc64-/' configure.ac configure

%build
%configure
sed -i.rpath 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i.rpath 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool
%{make_build} OPT_CFLAGS=""

%install
%{make_install}
install -pm 0644 libmpeg2/mpeg2_internal.h %{buildroot}%{_includedir}/mpeg2dec/
%delete_la

%post
/sbin/ldconfig
%postun
/sbin/ldconfig

%files
%doc AUTHORS ChangeLog NEWS README TODO COPYING
%{_libdir}/*.so.*

%files -n mpeg2dec
%{_bindir}/{corrupt_mpeg2,extract_mpeg2,mpeg2dec}
%{_mandir}/man1/*.1*

%files devel
%doc CodingStyle doc/libmpeg2.txt doc/sample*.c
%{_includedir}/mpeg2dec/
%{_libdir}/*.so
%{_libdir}/pkgconfig/{libmpeg2.pc,libmpeg2convert.pc}
%exclude %{_libdir}/{libmpeg2.a,libmpeg2convert.a}

%changelog
* Tue Jun 9 2020 leiju <leiju4@huawei.com> - 0.5.1-19
- Package init
